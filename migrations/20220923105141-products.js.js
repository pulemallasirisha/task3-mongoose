const groups = require("../models/products")

module.exports = {
    async up(db) {
        return await db.createCollection('products')
    },
    async down(db) {
        return await db.collection('products').drop()
    },
}