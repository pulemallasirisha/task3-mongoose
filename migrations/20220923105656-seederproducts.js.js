module.exports = {
  async up(db) {
      return await db.collection('products').insertMany([{
        name: 'lenovo thinkpad',
        description:'Lenovo ThinkPad is a Windows 10 laptop with a 14.00-inch display.',
        image_url:'https://upload.wikimedia.org/wikipedia/commons/4/45/Lenovo_ThinkPad_X1_Ultrabook.jpg',
        categoriesid:1,
        isactive:'true',
      },
      {
        name: 'Dell G7 17 (2020)',
        description:'It is powered by a Core i5 processor and it comes with 8GB of RAM.',
        image_url:'https://m.media-amazon.com/images/I/51lujnPZRwL._SX425_.jpg',
        categoriesid:2,
        isactive:'true',
      },
      {
        name: 'OPPO F19 Pro',
        description:'The Oppo F19 Pro is a decent mid-range smartphone with good display and battery life',
        image_url:'https://i0.wp.com/www.smartprix.com/bytes/wp-content/uploads/2021/03/1.jpg?fit=4032%2C3024&ssl=1',
        categoryiesid:3,
        isactive:'true',
      }], {})
  },
  async down(db) {
      return await db.collection('products').deleteMany({})
  },
}