module.exports = {
  async up(db) {
      return await db.collection('categories').insertMany([{
        name: "laptops",
        description: "A modern laptop is self-contained, with a screen, keyboard and pointing device (like a touchpad), plus usually, speakers, a microphone and a camera",
        groupid:1,
        Isactive: "true"
      },
      {
        name:'Dell',
        description:'The company designs, develops, manufactures, markets, sells, and supports information technology infrastructure such as laptops, desktops, mobiles,',
        groupId:1,
        isactive:'true'
      },
      {
        name:'oneplus',
        description:'OnePlus phones are known for their solid battery life and fast charging support,',
        groupId:2,
        isactive:'true',
      }], {})
  },
  async down(db) {
      return await db.collection('categories').deleteMany({})
  },
}