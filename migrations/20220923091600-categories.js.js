const groups = require("../models/categories")

module.exports = {
    async up(db) {
        return await db.createCollection('categories')
    },
    async down(db) {
        return await db.collection('categories').drop()
    },
}