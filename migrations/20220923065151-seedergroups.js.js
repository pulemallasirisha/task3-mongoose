module.exports = {
  async up(db) {
      return await db.collection('groups').insertMany([{
          name: "laptops",
          description: "A modern laptop is self-contained, with a screen, keyboard and pointing device (like a touchpad), plus usually, speakers, a microphone and a camera",
          Isactive: "true"
      },
      {
          name: "Mobiles",
          description: "A mobile phone is a wireless handheld device that allows users to make and receive calls'",
          Isactive: "true"
      }], {})
  },
  async down(db) {
      return await db.collection('groups').deleteMany({})
  },
}